
const Product = require("../models/Product");
const Order = require("../models/Order");

const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.addProduct = (data) => {
	if (data.isAdmin) {
		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
            isSteam: data.product.isSteam,
            isEpic: data.product.isEpic,
			price : data.product.price
		});
		return newProduct.save().then((product, error) => {
			if (error) {
				return false
			} else {
				return "Product added!"
			}
		})
	} else {
		return Promise.resolve(false + "Admin Only Functionalities");
	}
};

module.exports.getAllActiveProducts = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}


module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

module.exports.updateProduct = (reqParams, data) => {
    if(data.isAdmin) {
        let updatedProduct = {
            name : data.product.name,
			description : data.product.description,
			price : data.product.price
        }
        return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
            if(error) { 
                return false;
            }
            else {
                return true;
            }
        })
    }
    else {
        return Promise.resolve(false);
    }
}

module.exports.archiveProduct = (reqParams, data) => {
    if(data.isAdmin) {
        let archivedProduct = {
            isActive : false
        }
        return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product, error) => {
            if(error) {
                return false;
            }
            else{
                return true;
            }
        })
    }
    else {
        return Promise.resolve(false);
    }
}

// module.exports.getAllSteam = () => {
//     Product.find({isSteam : true}).then(result => {
//         return result;
//     })
// }

// module.exports.getAllEpic = () => {
//     Product.find({isEpic : true}).then(result => {
//         return result;
//     })
// }