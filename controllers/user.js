const User = require("../models/User");
const Order = require("../models/Order");
const Product = require("../models/Product");

const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if(result.length > 0) {
			return "Email exists";
		}
		else {
			return "Email does not exists";
		}
	})
}

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return "Error 404";
		}
		else {
			return "User Registered";
		}

	})

}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null) {
			return false
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect) {
				return {Hello : auth.createAccessToken(result)}
			}
			else {
				return "Error";
			}
		}
	})
}

module.exports.updateUserToAdmin = (reqParams, data) => {
		{
	    let updatedUserToAdmin = {
	        isAdmin : true
	    }
	    return User.findByIdAndUpdate(reqParams.userId, updatedUserToAdmin).then((user, error) => {
	        if(error){
	            return "Cannot Update User as Admin";
	        }
	        else {
	            return "Updated User as Admin";
	        }
	    })
	}
	
}

module.exports.createOrder = async (data) => {
	if (data.isAdmin) {
		return Promise.resolve("User Only Functionalities")
	} 
	else {
		let userCreateOrder = await Product.findById(data.productId).then(product => {
            if(product.isActive == true) {
                
                let newOrder = new Order({
                    userId: data.userId,
                    productId: data.productId,
                    quantity: data.quantity,
                    totalAmount: product.price * data.quantity
                })
                return newOrder.save().then((product, error) => {
                    if(error) {
                        return "Error";
                    }
                    else {
                        return "Order Created";
                    }
                })
            }
            else {
                return "Admin Only Functionalities";
            }
        })

        let isProductUpdated = await Product.findById(data.productId).then(product => {
            if(product.isActive == true) {
                product.orders.push({userId : data.userId})

                return product.save().then((product, error) => {
                    if(error) {
                        return "Error";
                    }
                    else {
                        return "Product Added";
                    }
                })
            }
            else {
                return "Product Inactive";
            }
        })

        let isUserUpdated = await User.findById(data.userId).then(user =>{
            user.orders.push({productId : data.productId})
            return user.save().then((user, error) => {
                 if(error) {
                     return "Error";
                 }
                 else {
                     return "Order Successfully Saved";
                 }
             })
         })
         if(userCreateOrder && isProductUpdated && isUserUpdated){
            return "Order is Successful";
         }
         else {
            return "Error";
         }
	};
};

module.exports.getAllOrders = (data) => {

	if(data.isAdmin) {

		return Order.find({}).then(result => {
			return result;
		})

	}
	else {
		return Promise.resolve("Admin Only Functionalities");
	}
	
}

module.exports.getMyOrders = (data) => {
	if(data.isAdmin) {
		return Promise.resolve("User Only Functionalities");
	}
	else {
		return Order.find({ userId : data.userId}).then(result => {
			return result;
		})
	}	
}