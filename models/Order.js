const mongoose = require("mongoose");
const orderSchema = new mongoose.Schema({

	userId : {
		type : String,
		required: [true, "User Id is required"]
	},
	productId : {
        type : String,
        required : [true, "Product Id is required."]
    },
    quantity : {
        type : Number,
		default : 1
    },
	totalAmount : {
		type : Number,
		required : [true, "Total Amount is required"]
	},
	purchasedOn : {
		type : Date,
		default : new Date()
	}

})


module.exports = mongoose.model("Order", orderSchema);
