const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");


router.post("/", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	console.log(data);
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});


router.get("/", (req, res) => {

	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController));
});


router.get("/:productId", (req, res) => {

	console.log(req.params.productId);
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


router.put("/:productId", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data);
	productController.updateProduct(req.params, data).then(resultFromController => res.send(resultFromController));
});


router.put("/:productId/archive", auth.verify, (req, res) => {

    const data = {
        product : req.body,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    };
    console.log(data);
    productController.archiveProduct(req.params, data).then(resultFromController => res.send(resultFromController))
});


module.exports = router;