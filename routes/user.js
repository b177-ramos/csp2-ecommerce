const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const productController = require("../controllers/product");
const auth = require("../auth");


router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});


router.post("/register", (req, res) => {
	
	console.log(req.body);
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

	const userData = {

        user : req.body,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }
    console.log(userData);
    userController.updateUserAsAdmin(req.params, userData).then(resultFromController => res.send(resultFromController))
});


router.post("/checkout", auth.verify, (req, res) => {

    const data = {
        userId : req.body.userId,
        productId : req.body.productId,
		quantity : req.body.quantity,     
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }
	userController.createOrder(data).then(resultFromController => res.send(resultFromController));
})


router.get("/orders", auth.verify, (req, res) => {

	const data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data);
	userController.getAllOrders(data).then(resultFromController => res.send(resultFromController));
});


router.get("/myOrders", auth.verify, (req, res) => {

	const data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data);
	userController.getMyOrders(data).then(resultFromController => res.send(resultFromController));
});


module.exports = router;