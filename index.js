const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");

const app = express();

mongoose.connect("mongodb+srv://admin:admin@cluster0.p2t9q.mongodb.net/csp2-ecommerce", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
mongoose.connection
	.once('open', () => console.log('Now connected to MongoDB Atlas'));
mongoose.connection
	.on("error", error => {console.log("No connection due to ", error)
	});


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/products", productRoutes);
app.use("/users", userRoutes);

app.listen(process.env.PORT || 3000, () => {
	console.log(`API is now online at port ${ process.env.PORT || 3000 }`)
})